package hazo.hafid.core;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import hazo.hafid.model.ClienteModel;
import hazo.hafid.model.ProdutoModel;
import hazo.hafid.model.UsuarioModel;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class Model
{   
    protected static String   daoTable = "";
    protected static String[][] daoColumns = {};
    public static DBSQLite connection = null;
    
    public Model(){
        Model.connection = new DBSQLite(Controller.context);
    }
    
    protected void setDaoTable( String table ){
        this.daoTable = table;
    }
    
    protected void setDaoColumns( String[][] cols ){
        this.daoColumns = cols;
    }
    
    public String[] createTable()
    {
        try{
            String sqlString = "CREATE TABLE IF NOT EXISTS " + this.daoTable + " (";
            for( int i = 0; i < this.daoColumns.length; i++ ){
                String[] col = this.daoColumns[i];
                if( i == 0 ){
                    sqlString += col[0] + " " + col[1] + " PRIMARY KEY AUTOINCREMENT";
                }else{
                    sqlString += ", " + col[0] + " " + col[1] + "";
                }
            }
            sqlString += " );";

            log("createTable", sqlString);
            
            SQLiteDatabase database = this.connection.getWritableDatabase();
            database.execSQL(sqlString);
            
        }catch( SQLException erro ){
            log("createTable", "createTable SQLException: " + erro.getMessage());
            return new String[]{"E", erro.getMessage()};
        }
        return new String[]{"S",""};
    }
    
    public String getString(String attribute)
    {
        try{
            Object o = this;
            Class<?> c = o.getClass();

            Field f = c.getDeclaredField( attribute );
            f.setAccessible(true);

            return (String) f.get(o);
        }catch( NoSuchFieldException e ){
            log("getString", "NoSuchFieldException: "+e.getMessage());
        }catch( IllegalAccessException e ){
            log("getString", "IllegalAccessException: "+e.getMessage());
        }catch( IllegalArgumentException e ){
            log("getString", "IllegalArgumentException: "+e.getMessage());
        }
        return null;
    }
    
    public Integer getInt(String attribute)
    {
        try{
            Object o = this;
            Class<?> c = o.getClass();

            Field f = c.getDeclaredField( attribute );
            f.setAccessible(true);

            return (Integer) f.get(o);
        }catch( NoSuchFieldException e ){
            log("getInt", e.getMessage());
        }catch( IllegalAccessException e ){
            log("getInt", e.getMessage());
        }catch( IllegalArgumentException e ){
            log("getInt", e.getMessage());
        }
        return null;
    }
    
    public void setString(String attribute, String value)
    {
        try{
            Object o = this;
            Class<?> c = o.getClass();

            Field f = c.getDeclaredField( attribute );
            f.setAccessible(true);

            f.set(o, value);
        }catch( NoSuchFieldException e ){
            log("setString", "NoSuchFieldException: "+e.getMessage());
        }catch( IllegalAccessException e ){
            log("setString", "IllegalAccessException: "+e.getMessage());
        }catch( IllegalArgumentException e ){
            log("setString", "IllegalArgumentException: "+e.getMessage());
        }
    }
    
    public void setInt(String attribute, Integer value)
    {
        try{
            Object o = this;
            Class<?> c = o.getClass();

            Field f = c.getDeclaredField( attribute );
            f.setAccessible(true);

            f.set(o, value);
        }catch( NoSuchFieldException e ){
            log("setInt", "Mode.setInt() NoSuchFieldException: "+e.getMessage());
        }catch( IllegalAccessException e ){
            log("setInt", "Mode.setInt() IllegalAccessException: "+e.getMessage());
        }catch( IllegalArgumentException e ){
            log("setInt", "Mode.setInt() IllegalArgumentException: "+e.getMessage());
        }
    }
    
    public String[] save(String type)
    {
        Integer primaryValue = this.getInt(this.daoColumns[0][2]);
        
        if( type.length() > 0 ){
            if( type.equals("I") ){
                return this.insert();
            }else if( type.equals("U") ){
                return this.update();
            }else if( type.equals("R") ){
                return this.replace();
            }else if( type.equals("D") ){
                return this.delete();
            }else{
                return new String[]{"E", "Operação inválida"};
            }
        }else{
            if( primaryValue.equals("null") || primaryValue < 1 ){
                return this.insert();
            }else{
                return this.update();
            }
        }
    }
    
    public String[] save()
    {
        return this.save("");
    }
    
    private String[] insert()
    {
        SQLiteDatabase database = this.connection.getWritableDatabase();
        
        ContentValues valores = new ContentValues();
        
        for( int i = 1; i < this.daoColumns.length; i++ ){
            String[] col = this.daoColumns[i];
            if( col[1].equals("INTEGER") ){
                valores.put(col[0], this.getInt(col[2]));
            }else{
                valores.put(col[0], this.getString(col[2]));
            }
                
        }
        
        long result = database.insert(this.daoTable, null, valores); 
        database.close();
        
        if( result > 0 ){
            this.setInt(this.daoColumns[0][2], (int) result);
            return new String[]{"S"};
        }else{
            return new String[]{"E", "Falha ao inserir registro"};
        }
    }
    
    private String[] update()
    {
        SQLiteDatabase database = this.connection.getWritableDatabase();
        
        ContentValues valores = new ContentValues();
        
        for( int i = 0; i < this.daoColumns.length; i++ ){
            String[] col = this.daoColumns[i];
            if( col[1].equals("INTEGER") ){
                valores.put(col[0], this.getInt(col[2]));
            }else{
                valores.put(col[0], this.getString(col[2]));
            } 
        }
        
        String comparacaoString = this.daoColumns[0][0]+" = ?";
        String[] comparacaoValores = { this.getInt(this.daoColumns[0][2]).toString() };
        
        long result = database.update(this.daoTable, valores, comparacaoString, comparacaoValores); 
        database.close();
        
        if( result > 0 ){
            return new String[]{"S"};
        }else{
            return new String[]{"E", "Falha ao atualizar registro"};
        }
    }
    
    private String[] replace()
    {
        SQLiteDatabase database = this.connection.getWritableDatabase();
        
        ContentValues valores = new ContentValues();
        
        for( int i = 0; i < this.daoColumns.length; i++ ){
            String[] col = this.daoColumns[i];
            if( col[1].equals("INTEGER") ){
                valores.put(col[0], this.getInt(col[2]));
            }else{
                valores.put(col[0], this.getString(col[2]));
            } 
        }
        
        long result = database.replace(this.daoTable, null, valores);
        database.close();
        
        if( result > 0 ){
            return new String[]{"S"};
        }else{
            return new String[]{"E", "Falha ao atualizar registro"};
        }
    }
    
    public String[] delete()
    {
        SQLiteDatabase database = this.connection.getWritableDatabase();
        
        String where = this.daoColumns[0][0]+" = ?";
        String[] bind = {this.getInt(this.daoColumns[0][2]).toString()};
        
        long result = database.delete(this.daoTable, where, bind); 
        database.close();
        
        if( result > 0 ){
            return new String[]{"S"};
        }else{
            return new String[]{"E", "Falha ao excluir registro"};
        }
    }
    
    public void load()
    {
        String sqlColunas = "";
        for( int i = 0; i < this.daoColumns.length; i++ ){
            String[] col = this.daoColumns[i];
            sqlColunas += (i == 0) ? "" : ", ";
            sqlColunas += col[0];
        }
        SQLiteDatabase database = this.connection.getWritableDatabase(); 
        String sql = "SELECT "+sqlColunas+" FROM " + this.daoTable 
                + " where " + this.daoColumns[0][0] + " = " + this.getInt(this.daoColumns[0][2]);

        log("load", sql);
        Cursor cursor = database.rawQuery(sql, null);

        if( cursor.moveToFirst() ){
            do{
                for( int i = 0; i < this.daoColumns.length; i++ ){
                    String[] col = this.daoColumns[i];
                    int columnIndex = cursor.getColumnIndex(col[0]);
                    if( col[1].equals("INTEGER") ){
                        this.setInt(col[2], Integer.parseInt(cursor.getString(columnIndex)) );
                    }else{
                        this.setString(col[2], cursor.getString(columnIndex));
                    } 
                }
                
            }while( cursor.moveToNext() );
        }
    }
    
    public void load( int primaryValue )
    {
        this.setInt(this.daoColumns[0][2], primaryValue);
        this.load();
    }
    
    public String toString()
    {
        String result = "########## Tabela \"" + this.daoTable + "\" ##########\r\n";
        for( String[] col : this.daoColumns ){
            result += col[0]+": ";
            if( col[1].equals("INTEGER") ){
                result += this.getInt(col[2]).toString();
            }else{
                result += this.getString(col[2]);
            }
            result += "\r\n";
        }
        result += "##########";
        return result;
    }
    
    public ArrayList getList()
    {
        return this.getList("", "", 0, -1);
    }
    
    public ArrayList getList( String where )
    {
        return this.getList(where, "", 0, -1);
    }
    
    public ArrayList getList( String where, String order )
    {
        return this.getList(where, order, 0, -1);
    }
    
    public ArrayList getList( String where, String order, Integer rowStart, Integer rowLimit )
    {
        String sqlColunas = "";
        for( int i = 0; i < Model.daoColumns.length; i++ ){
            String[] col = Model.daoColumns[i];
            sqlColunas += (i == 0) ? "dao." : ", dao.";
            sqlColunas += col[0];
        }
        SQLiteDatabase database = Model.connection.getWritableDatabase(); 
        String sql = "SELECT "+sqlColunas+" FROM " + Model.daoTable+" dao";
        
        if( where.length() > 0 ){
            sql += " where " + where;
        }
        
        if( order.length() > 0 ){
            sql += " order by " + order;
        }
        
        if( rowLimit != -1 ){
            sql += " limit " + rowStart.toString() + ", " + rowLimit.toString();
        }
        
        log("getList",sql);
        Cursor response = database.rawQuery(sql, null);
        
        ArrayList<Model> lista = new ArrayList<Model>();
        
        try{
            if( response.moveToFirst() ){
                
                String className = this.getClass().getName();
                Class classe = Class.forName( className );
                
                do{
                    Object nModel = classe.newInstance();
                    
                    Class<?> c = nModel.getClass();

                    Field f = c.getDeclaredField( this.daoColumns[0][2] );
                    f.setAccessible(true);
                    int columnIndex = response.getColumnIndex( this.daoColumns[0][0] );
                    f.set(nModel, Integer.parseInt(response.getString(columnIndex)));
                    
                    Method m = c.getMethod("load");
                    m.invoke(nModel);

                    lista.add( (Model) nModel );
                    
                }while( response.moveToNext() );
            }
        }catch( Exception e ){
            log("getList", "Exception: "+e.getMessage());
        }
        return lista;
    }
    
    public void dropTable()
    {
        SQLiteDatabase database = this.connection.getWritableDatabase();
        String sql = "DROP TABLE IF EXISTS "+this.daoTable;
        log("dropTable", sql);
        database.execSQL(sql); 
        database.close();
    }
    
    static public void createAllModels()
    {
//        new ClienteModel().dropTable();
        new ClienteModel().createTable();
//        new UsuarioModel().dropTable();
        new UsuarioModel().createTable();
//        new ProdutoModel().dropTable();
        new ProdutoModel().createTable();
        
        UsuarioModel Usuario = new UsuarioModel();
        
        ArrayList<UsuarioModel> list = Usuario.getList("dao.login = ''");
        if( list.size() <= 0 ){
            Usuario.id = 1;
            Usuario.nome = "Fernando Gurkievicz";
            Usuario.login = "fernando";
            Usuario.senha = "fernando";
            Usuario.save("R");
        }
    }
    
    public void log( String metodo,  String string )
    {
        Log.d("DATABASE QUERY", metodo+": "+string);
    }
}
