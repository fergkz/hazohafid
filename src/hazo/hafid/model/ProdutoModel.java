package hazo.hafid.model;

import hazo.hafid.core.Model;

public class ProdutoModel extends Model
{
    public Integer id = 0;
    public String descricao = "";
    public String preco = "";
    public String GTIN = "";
    
    public ProdutoModel()
    {
        super();
        this.setDaoTable("produto");
        String[][] cols = {
            //coluna             tipo       atributo referenciado
            { "id",        "INTEGER", "id" },
            { "descricao", "TEXT",    "descricao" },
            { "preco",     "TEXT",    "preco" },
            { "GTIN",      "TEXT",    "GTIN" }
        };
        super.setDaoColumns(cols);
    }
    
    @Override
    public String[] save( String type )
    {
        if( this.descricao.length() < 1 ){
            return new String[]{"E","A descrição do produto deve ser informada"};
        }
        
        return super.save(type);
    }
}
