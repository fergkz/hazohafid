package hazo.hafid.model;

import hazo.hafid.core.Model;

public class UsuarioModel extends Model
{
    public Integer id = 0;
    public String nome;
    public String login;
    public String senha;
    
    public UsuarioModel()
    {
        super();
        this.setDaoTable("usuario");
        String[][] cols = {
            //coluna             tipo       atributo referenciado
            { "id",     "INTEGER", "id" },
            { "nome",   "TEXT",    "nome" },
            { "login",  "TEXT",    "login" },
            { "senha",  "TEXT",    "senha" }
        };
        super.setDaoColumns(cols);
    }
}
