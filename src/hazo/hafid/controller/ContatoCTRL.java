package hazo.hafid.controller;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import hazo.hafid.core.Controller;
import hazo.hafid.model.ClienteModel;
import hazo.hafid.model.ContatoModel;

public class ContatoCTRL extends Controller {

    public void getContatoAction( Bundle savedInstanceState )
    {
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        Intent intent = new Intent( Intent.ACTION_PICK, uri );
        startActivityForResult(intent, 0);
    }
        
    @Override
    protected void onActivityResult( int codigo, int resultado, Intent intent ){
        super.onActivityResult(codigo, resultado, intent);
        
        Uri uri = intent.getData();
        
        ContatoModel Contato = ContatoModel.getByUri( uri );
        
        String[] nomeArray = Contato.nome.split(" ");
        
        ClienteModel Cliente = new ClienteModel();
        Cliente.idAndroid = Contato.id;
        Cliente.nome = nomeArray[0];
        Cliente.sobrenome = "";
        
        if( nomeArray.length > 1 ){
            for( int i = 1; i < nomeArray.length; i++ ){
                Cliente.sobrenome += (i > 1 ? " " : "") + nomeArray[i];
            }
        }
            
        if( Contato.telefones != null ){
            Cliente.numero = Contato.telefones.get(0);
        }
        Cliente.save();
        
        startAction( ClienteCTRL.class, "cadastroAction", Cliente.id);
    }
    
}
