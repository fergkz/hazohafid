package hazo.hafid.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DBSQLite extends SQLiteOpenHelper{
    
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "HazoHafidDatabase";

    public DBSQLite(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase banco) {
        // SQL de Cria��o da tabela de grupo
        /*
        String CREATE_BOOK_TABLE = "CREATE TABLE tb_grupo ( "
                + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "nome TEXT )";
        // Criando a table de grupo
        banco.execSQL(CREATE_BOOK_TABLE);
        */
    }

    @Override
    public void onUpgrade(SQLiteDatabase banco, int arg1, int arg2) {
        /*
        // Apagando table antiga caso exista
        banco.execSQL("DROP TABLE IF EXISTS tb_grupo");

        // Criar a nova tabela, chamando o onCreate
        this.onCreate(banco);
        */
    }

}
