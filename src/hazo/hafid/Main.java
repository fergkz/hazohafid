package hazo.hafid;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import hazo.hafid.controller.SessaoCTRL;
import hazo.hafid.core.Controller;
import hazo.hafid.core.Model;
import jim.h.common.android.zxinglib.integrator.IntentIntegrator;
import jim.h.common.android.zxinglib.integrator.IntentResult;

public class Main extends Controller
{   
    @Override
    public void onCreate(Bundle savedInstanceState)
    {   
        super.onCreate(savedInstanceState);
        
//        Intent intent = new Intent(getInstance(), ProdutoScanditCTRL.class);
//        startActivity(intent);
        
//        IntentIntegrator integrator = new IntentIntegrator(yourActivity);
//        integrator.initiateScan();
        
//        IntentIntegrator.initiateScan(  Main.this,
//                        R.layout.zxinglib,
//                        R.id_capture.viewfinder_view,
//                        R.id_capture.preview_view,
//                        false );
        
        
        Model.createAllModels();
        finish();
        startAction( SessaoCTRL.class, "indexAction" );
    }
    
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            case IntentIntegrator.REQUEST_CODE:
//                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
//                final String result = scanResult.getContents();
//                if ((result != null) && (scanResult.getFormatName().toString().contentEquals("EAN_13"))) {
//
//                    log("RESULTADO COM SUCESSO");
//                } else {
//                    log("Código inválido ou inexistente.");
//                }
//                break;
//            default:
//        }
//    }
    
}
