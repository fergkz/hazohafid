package hazo.hafid.core;

import android.app.Activity;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
//import android.util.Log;
//import android.widget.Toast;
import hazo.hafid.controller.helper.Sistema;
//import hazo.hafid.model.ClienteModel;

public class ModelDAO extends Activity
{
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "HazoHafidDatabase";
    
    protected String   daoTable = "";
    protected String[][] daoColumns = {};
    public static SQLiteDatabase connection = null;
    
    /*public void onCreate(Bundle savedInstanceState)
    {
        raise("onCreate");
        this.connection = connection;
        this.createTable();
    }*/
    
    public SQLiteDatabase openConnection()
    {
        try{
            connection = openOrCreateDatabase(DB_NAME, MODE_PRIVATE, null);
            String sql = "CREATE TABLE IF NOT EXISTS pessoas(codigo INTEGER PRIMARY KEY, login TEXT, senha TEXT);";
            connection.execSQL(sql);
            
            Sistema.log("Banco Criado!");
        }catch( SQLException e ){
            Sistema.log("connect Erro: " + e.getMessage());
        }
        return connection;
    }
    
    protected void setDaoTable( String table ){
        this.daoTable = table;
    }
    
    protected void setDaoColumns( String[][] cols ){
        this.daoColumns = cols;
    }
    
//    public boolean createTable()
//    {
//        raise("createTable");
//        try{
//            String sqlString = "CREATE TABLE IF NOT EXISTS " + this.daoTable + " (";
//            for( int i = 0; i < this.daoColumns.length; i++ ){
//                String[] col = this.daoColumns[i];
//                if( i == 0 ){
//                    sqlString += col[0] + " " + col[1] + " PRIMARY KEY";
//                }else{
//                    sqlString += ", " + col[0] + " " + col[1] + "";
//                }
//            }
//            sqlString += " );";
//
//            this.raise(sqlString);
//            
//            this.connection.execSQL(sqlString);
//            raise("CRIADO");
//        }catch( SQLException erro ){
//            this.raise("createTable SQLException: " + erro.getMessage());
//            return false;
//        }catch( Exception erro ){
//            this.raise("createTable Exception: " + erro.getMessage());
//            return false;
//        }
//        return true;
//    }
    /*
    SQLiteDatabase BancoDeDados = null;
    public void connect()
    {
        try{
            BancoDeDados = openOrCreateDatabase("TMP1.db", MODE_PRIVATE, null);
            String sql = "CREATE TABLE IF NOT EXISTS cliente(codigo INTEGER PRIMARY KEY, login TEXT, senha TEXT);";
            BancoDeDados.execSQL(sql);
            
            this.raise("Banco Criado!");
        }catch( Exception e ){
            this.raise("connect Erro: " + e.getMessage());
        }
    }
    */
//    static public void createAllModels()
//    {
//        ClienteModel Cliente = new ClienteModel();
//        Cliente.openConnection();
        
//        new ClienteModel();
//    }
/*
    @Override
    public void onCreate( SQLiteDatabase sqld ){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqld, int i, int i1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
*/
}
