package hazo.hafid.controller;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import hazo.hafid.R;
import hazo.hafid.core.Controller;
import hazo.hafid.model.ClienteModel;
import java.util.ArrayList;

public class ClienteCTRL extends Controller
{
    String enderecoCliente = "";
    
    public ArrayList<ClienteModel> clientes = null;
    
    public void listaAction( Bundle savedInstanceState )
    {
        setView(R.layout.cliente_lista);
        
        clientes = new ClienteModel().getList("","dao.nome asc");
        
        String[] clientesStr = new String[clientes.size()];
        
        for( int i = 0; i < clientes.size(); i++ ){
            ClienteModel Cliente = clientes.get(i);
            clientesStr[i] = Cliente.nome + " " + Cliente.sobrenome;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
            this, 
            android.R.layout.simple_list_item_1,
            clientesStr
        );
        
        ListView lv = (ListView)findViewById(R.id.cliente_lista_list);
        lv.setAdapter(adapter);
        
        
        /************** AÇÕES DOS BOTÕES ****************/
        lv.setOnItemClickListener(new OnItemClickListener(){
            public void onItemClick(AdapterView<?> av, View view, int position, long id) {
                finish();
                startAction( ClienteCTRL.class, "cadastroAction", clientes.get(position).id );
            }
        });
        
        Button button = (Button) findViewById(R.id.cliente_lista_btn_novo);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( ClienteCTRL.class, "cadastroAction");
            }
        });

        button = (Button) findViewById(R.id.cliente_lista_btn_voltar);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( MainCTRL.class, "indexAction");
            }
        });

    }
    
    public void cadastroAction( Bundle savedInstanceState )
    {
        setView(R.layout.cliente_cadastro);
        
        Intent intent = getIntent();
        Integer idCliente = intent.getIntExtra("param", 0);
        intent.removeExtra("param");
        
        final ClienteModel Cliente = new ClienteModel();
        Cliente.id = idCliente;
        Cliente.load();
        
        Button btnGetContato = (Button) findViewById(R.id.cliente_cadastro_get_contato);
        Button btnShowMaps = (Button) findViewById(R.id.cliente_cadastro_show_maps);
        Button btnExcluir = (Button) findViewById(R.id.cliente_cadastro_excluir_btn);
        Button btnSalvar = (Button) findViewById(R.id.cliente_cadastro_salvar_btn);
        Button btnVoltar = (Button) findViewById(R.id.cliente_cadastro_voltar_btn);
        
        // Se for novo
        if( Cliente.id <= 0 ){
            btnShowMaps.setVisibility( View.GONE );
            btnExcluir.setVisibility( View.GONE );
            btnGetContato.setVisibility( View.VISIBLE );
        }else{
            btnGetContato.setVisibility( View.GONE  );
            this.enderecoCliente = Cliente.endEndereco;
            btnExcluir.setVisibility( View.VISIBLE );
            
            if( Cliente.endEndereco.length() > 2 ){
                btnShowMaps.setVisibility( View.VISIBLE  );
            }else{
                btnShowMaps.setVisibility( View.GONE );
            }
        }
        
        btnGetContato.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( ContatoCTRL.class, "getContatoAction");
            }
        });
        
        
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                    startAction( ClienteCTRL.class, "listaAction");
            }
        });
        
        btnShowMaps.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                enderecoCliente = enderecoCliente.replace(" ","+").replace(",","").replace(".","").toLowerCase();
                Uri uri1 = Uri.parse("geo:0,0?q="+enderecoCliente);
                Intent it1= new Intent(Intent.ACTION_VIEW, uri1);
                startActivity(it1);
            }
        });
        
        EditText inputNome = (EditText) findViewById(R.id.cliente_cadastro_nome_input);
        inputNome.setText(Cliente.nome);
        
        EditText inputSobrenome = (EditText) findViewById(R.id.cliente_cadastro_sobrenome_input);
        inputSobrenome.setText(Cliente.sobrenome);
        
        EditText inputNumero = (EditText) findViewById(R.id.cliente_cadastro_telefone_input);
        inputNumero.setText(Cliente.numero);
        
        EditText inputDataNasc = (EditText) findViewById(R.id.cliente_cadastro_dt_nasc_input);
        inputDataNasc.setText(Cliente.dataNascimento);
        
        EditText inputEndCep = (EditText) findViewById(R.id.cliente_cadastro_end_cep_input);
        inputEndCep.setText(Cliente.endCEP);
        
        EditText inputEndEndereco = (EditText) findViewById(R.id.cliente_cadastro_end_endereco_input);
        inputEndEndereco.setText(Cliente.endEndereco);
        
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                EditText inputNome        = (EditText) findViewById( R.id.cliente_cadastro_nome_input );
                EditText inputSobrenome   = (EditText) findViewById( R.id.cliente_cadastro_sobrenome_input );
                EditText inputTelefone    = (EditText) findViewById( R.id.cliente_cadastro_telefone_input );
                EditText inputDataNasc    = (EditText) findViewById( R.id.cliente_cadastro_dt_nasc_input );
                EditText inputEndCep      = (EditText) findViewById( R.id.cliente_cadastro_end_cep_input );
                EditText inputEndEndereco = (EditText) findViewById( R.id.cliente_cadastro_end_endereco_input );
                
                Cliente.nome           = inputNome.getText().toString();
                Cliente.numero         = inputTelefone.getText().toString();
                Cliente.sobrenome      = inputSobrenome.getText().toString();
                Cliente.dataNascimento = inputDataNasc.getText().toString();
                Cliente.endCEP         = inputEndCep.getText().toString();
                Cliente.endEndereco    = inputEndEndereco.getText().toString();
                
                String[] retorno = Cliente.save();
                
                if( retorno[0].equals("S") ){
                    message("Cliente salvo com sucesso", "S");
                    finish();
                    startAction( ClienteCTRL.class, "listaAction");
                }else{
                    message(retorno[1]);
                }
            }
        });
        
        btnExcluir.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                String[] retorno = Cliente.delete();
                
                if( retorno[0].equals("S") ){
                    message("Cliente excluído com sucesso", "S");
                    finish();
                    startAction( ClienteCTRL.class, "listaAction");
                }else{
                    message(retorno[1]);
                }
            }
        });
    }
    
}
