package hazo.hafid.controller.helper;

import hazo.hafid.core.Controller;
import java.io.IOException;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class WSActivity
{
    public static String[] obterLogradouroAuth( String cep )
    {
        String namespace = "http://www.byjg.com.br";
        String metodo    = "obterLogradouroAuth";
        String url       = "http://www.byjg.com.br/site/webservice.php/ws/cep";
        
        SoapObject requisicaoSoap = new SoapObject(namespace, metodo);
        
        requisicaoSoap.addProperty("cep", cep);
        requisicaoSoap.addProperty("usuario", "hazohafid");
        requisicaoSoap.addProperty("senha", "hazohafid");
        
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope( SoapEnvelope.VER11 );
        envelope.setOutputSoapObject(requisicaoSoap);
        
        HttpTransportSE httpTransport = new HttpTransportSE(url);
        
        try{
            httpTransport.call("", envelope);
            Object msg = envelope.getResponse();
            
            if( msg.toString().contains(",") ){
                return new String[]{"S", msg.toString()};
            }else{
                Sistema.log( msg.toString() );
            }
            
        }catch( IOException e ){
            Sistema.log( e.getMessage() );
        }catch( XmlPullParserException e ){
            Sistema.log( e.getMessage() );
        }
        
        return new String[]{"E", "O endereço para o CEP informando não foi encontrado"};
    }
    
}
