package hazo.hafid.model;

import android.database.Cursor;
import android.provider.ContactsContract;
import hazo.hafid.controller.helper.WSActivity;
import hazo.hafid.core.Controller;
import hazo.hafid.core.Model;
import java.util.ArrayList;

public class ClienteModel extends Model
{
    public Integer id = 0;
    public String idAndroid = "";
    public String nome = "";
    public String sobrenome = "";
    public String numero = "";
    public String dataNascimento = "";
    public String endCEP = "";
    public String endCidade = "";
    public String endEndereco = "";
    public String endNumero = "";
    
    
    public ClienteModel()
    {
        super();
        this.setDaoTable("cliente");
        String[][] cols = {
            //coluna             tipo       atributo referenciado
            { "id",              "INTEGER", "id" },
            { "id_android",      "TEXT",    "idAndroid" },
            { "nome",            "TEXT",    "nome" },
            { "sobrenome",       "TEXT",    "sobrenome" },
            { "numero",          "TEXT",    "numero" },
            { "data_nascimento", "TEXT",    "dataNascimento" },
            { "end_cep",         "TEXT",    "endCEP" },
            { "end_cidade",      "TEXT",    "endCidade" },
            { "end_endereco",    "TEXT",    "endEndereco" },
            { "end_numero",      "TEXT",    "endNumero" },
        };
        super.setDaoColumns(cols);
    }
    
    @Override
    public String[] save()
    {   
        if( this.endCEP.length() > 0 ){
            String[] endereco = WSActivity.obterLogradouroAuth(this.endCEP);
            if( endereco[0] == "S" ){
                if( this.endEndereco.length() <= 0 ){
                    this.endEndereco = endereco[1];
                }
            }
        }
        
        if( this.nome.length() < 1 ){
            return new String[]{"E", "O nome do cliente deve ser informado"};
        }
        
        return super.save();
    }
    
    public void loadByIdAndroid()
    {   
        ArrayList<ClienteModel> lista = this.getList("id_android = '"+this.idAndroid+"'");
        if( lista.size() > 0 ){
            this.id = lista.get(0).id;
            this.load();
        }
    }
    
    public static void importaFromContatos()
    {
        Cursor c = Controller.context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        
        int idIndex = c.getColumnIndex(ContactsContract.Contacts._ID);
        int hasNumberIndex = c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        int nameIndex = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        
        while( c.moveToNext() ){
            
            ClienteModel Cliente = new ClienteModel();
            Cliente.idAndroid = c.getString(idIndex);
            Cliente.loadByIdAndroid();
            Cliente.nome      = c.getString(nameIndex);

            if( c.getInt(hasNumberIndex) > 0 ){
                Cursor pCur = Controller.context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { Cliente.idAndroid },null);
                while( pCur.moveToNext() ){
                    Cliente.numero = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                } 
            }
            
            Cliente.save();
        }
    }

}
