package hazo.hafid.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import hazo.hafid.R;
import hazo.hafid.core.Controller;
import hazo.hafid.model.ProdutoModel;
import java.util.ArrayList;
import jim.h.common.android.zxinglib.integrator.IntentIntegrator;
import jim.h.common.android.zxinglib.integrator.IntentResult;

public class ProdutoCTRL extends Controller
{
    EditText inputDescricao;
    EditText inputPreco;
    EditText inputGTIN;
    
    public ArrayList<ProdutoModel> produtos = null;
    
    public void listaAction( Bundle savedInstanceState )
    {
        setView(R.layout.produto_lista);
        
        produtos = new ProdutoModel().getList("","dao.descricao asc");
        
        String[] produtosStr = new String[produtos.size()];
        
        for( int i = 0; i < produtos.size(); i++ ){
            ProdutoModel Produto = produtos.get(i);
            produtosStr[i] = Produto.descricao;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
            this, 
            android.R.layout.simple_list_item_1,
            produtosStr
        );
        
        ListView lv = (ListView)findViewById(R.id.produtos_lista_list);
        lv.setAdapter(adapter);
        
        
        /************** AÇÕES DOS BOTÕES ****************/
        lv.setOnItemClickListener(new OnItemClickListener(){
            public void onItemClick(AdapterView<?> av, View view, int position, long id) {
                finish();
                startAction( ProdutoCTRL.class, "cadastroAction", produtos.get(position).id );
            }
        });
        
        Button button = (Button) findViewById(R.id.produtos_lista_novo_produto_btn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( ProdutoCTRL.class, "cadastroAction");
            }
        });

        button = (Button) findViewById(R.id.produtos_lista_voltar_btn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( MainCTRL.class, "indexAction");
            }
        });

    }

    public void cadastroAction( Bundle savedInstanceState )
    {
        setView(R.layout.produto_cadastro);
        
        Intent intent = getIntent();
        Integer idProduto = intent.getIntExtra("param", 0);
        intent.removeExtra("param");
        
        final ProdutoModel Produto = new ProdutoModel();
        Produto.id = idProduto;
        Produto.load();
        
        Button btnVoltar  = (Button) findViewById(R.id.produto_cadastro_voltar_btn);
        Button btnSalvar  = (Button) findViewById(R.id.produto_cadastro_salvar_btn);
        Button btnExcluir = (Button) findViewById(R.id.produto_cadastro_excluir_btn);
        Button btnBarCode = (Button) findViewById(R.id.produto_cadastro_bar_code_btn);
        
        
        if( Produto.id <= 0 ){
            btnExcluir.setVisibility( View.GONE );
        }
        
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( ProdutoCTRL.class, "listaAction");
            }
        });
        
        inputDescricao = (EditText) findViewById(R.id.produto_cadastro_input_descricao);
        inputDescricao.setText(Produto.descricao);
        
        inputPreco = (EditText) findViewById(R.id.produto_cadastro_input_preco);
        inputPreco.setText(Produto.preco);
        
        inputGTIN = (EditText) findViewById(R.id.produto_cadastro_input_gtin);
        inputGTIN.setText(Produto.GTIN);
        
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                
                Produto.descricao = inputDescricao.getText().toString();
                Produto.preco     = inputPreco.getText().toString();
                Produto.GTIN      = inputGTIN.getText().toString();
                
                String[] retorno = Produto.save();
                
                if( retorno[0].equals("S") ){
                    message("Produto salvo com sucesso", "S");
                    finish();
                    startAction( ProdutoCTRL.class, "listaAction");
                }else{
                    message(retorno[1]);
                }
            }
        });
        
        btnExcluir.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                String[] retorno = Produto.delete();
                
                if( retorno[0].equals("S") ){
                    message("Produto excluído com sucesso", "S");
                    finish();
                    startAction( ProdutoCTRL.class, "listaAction");
                }else{
                    message(retorno[1]);
                }
            }
        });
        
        btnBarCode.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                IntentIntegrator.initiateScan(  ProdutoCTRL.this,
                        R.layout.zxinglib,
                        R.id_capture.viewfinder_view,
                        R.id_capture.preview_view,
                        false );
            }
        });
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                final String result = scanResult.getContents();
                    inputGTIN = (EditText) findViewById(R.id.produto_cadastro_input_gtin);
                    inputGTIN.setText( scanResult.getContents().toString() );
                break;
            default:
        }
    }
    
}