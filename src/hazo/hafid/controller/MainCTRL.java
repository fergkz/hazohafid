package hazo.hafid.controller;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import hazo.hafid.R;
import hazo.hafid.core.Controller;

public class MainCTRL extends Controller
{
    Button button;
    
    public void indexAction(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setView(R.layout.main_index);
        
        button = (Button) findViewById(R.id.main_btn_session_logoff);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( UsuarioCTRL.class, "logoffAction" );
            }
        });
        
        button = (Button) findViewById(R.id.main_btn_usuarios);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( UsuarioCTRL.class, "listaAction" );
            }
        });
        
        button = (Button) findViewById(R.id.main_btn_produtos);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( ProdutoCTRL.class, "listaAction" );
            }
        });
        
        button = (Button) findViewById(R.id.main_btn_clientes);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                finish();
                startAction( ClienteCTRL.class, "listaAction" );
            }
        });
        
    }
}
