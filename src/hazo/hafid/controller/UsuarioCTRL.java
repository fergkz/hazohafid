package hazo.hafid.controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import hazo.hafid.Main;
import hazo.hafid.R;
import hazo.hafid.core.Controller;
import hazo.hafid.model.Sessao;
import hazo.hafid.model.UsuarioModel;
import java.util.ArrayList;

public class UsuarioCTRL extends Controller
{   
    public void loginAction(Bundle savedInstanceState)
    {
        setView(R.layout.usuario_login);
        
        final Button button = (Button) findViewById(R.id.btn_entrar);
        
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText tlogin = (EditText) findViewById( R.id.usuario_login_login_input );
                EditText tsenha = (EditText) findViewById( R.id.usuario_login_senha_input );
                
                String login = tlogin.getText().toString();
                String senha = tsenha.getText().toString();
                
                if( Sessao.validaUsuario(login, senha) == true ){
                    Sessao.usuarioLogado = true;
                    startAction( SessaoCTRL.class, "indexAction" );
                }else{
                    message("Usuário ou senha inválidos");
                }
            }
        });
    }
    
    public void logoffAction(Bundle savedInstanceState)
    {
        Sessao.usuarioLogado = false;
        startAction( SessaoCTRL.class, "indexAction" );
    }
    
    public void listaAction(Bundle savedInstanceState)
    {
        setView(R.layout.usuario_lista);

        final ArrayList<UsuarioModel> listaUsuarios = new UsuarioModel().getList();
        String[] usuarios = new String[ listaUsuarios.size() ];
        
        for( Integer i = 0; i < listaUsuarios.size(); i++ ){
            UsuarioModel Usuario = listaUsuarios.get(i);
            usuarios[i] = Usuario.nome;
        }
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
            this, 
            android.R.layout.simple_list_item_1,
            usuarios
        );

        ListView lv = (ListView)findViewById(R.id.usuario_lista_list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new OnItemClickListener(){
            public void onItemClick(AdapterView<?> av, View view, int position, long id) {
                UsuarioModel UsuarioSelecionado;
                UsuarioSelecionado = listaUsuarios.get(position);
                startAction( UsuarioCTRL.class, "cadastroAction", UsuarioSelecionado.id);
            }
        });

        Button button = (Button) findViewById(R.id.usuario_cadastro_btn_novo);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                startAction( UsuarioCTRL.class, "cadastroAction");
            }
        });

        button = (Button) findViewById(R.id.usuario_cadastro_btn_voltar);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                startAction( MainCTRL.class, "indexAction");
            }
        });
    }
    
    public void cadastroAction(Bundle savedInstanceState)
    {
        Intent intent = getIntent();
        Integer idUsuario = intent.getIntExtra("param", 0);
        intent.removeExtra("param");
        
        final UsuarioModel Usuario = new UsuarioModel();
        Usuario.id = idUsuario;
        Usuario.load();

        setView(R.layout.usuario_cadastro);
        
        EditText inputNome = (EditText) findViewById(R.id.usuario_cadastro_nome_input);
        inputNome.setText(Usuario.nome);
        
        EditText inputLogin = (EditText) findViewById(R.id.usuario_cadastro_login_input);
        inputLogin.setText(Usuario.login);
        
        EditText inputSenha = (EditText) findViewById(R.id.usuario_cadastro_senha_input);
        inputSenha.setText(Usuario.senha);
        
        
        final Button button = (Button) findViewById(R.id.usuario_cadastro_salvar_btn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick( View v ){
                EditText inputNome  = (EditText) findViewById( R.id.usuario_cadastro_nome_input );
                EditText inputLogin = (EditText) findViewById( R.id.usuario_cadastro_login_input );
                EditText inputSenha = (EditText) findViewById( R.id.usuario_cadastro_senha_input );
                
                Usuario.nome  = inputNome.getText().toString();
                Usuario.login = inputLogin.getText().toString();
                Usuario.senha = inputSenha.getText().toString();
                Usuario.save();
                
                log("Usuário alterado com sucesso");
                startAction( UsuarioCTRL.class, "listaAction" );
            }
        });
    }
    
}
