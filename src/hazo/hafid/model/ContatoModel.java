package hazo.hafid.model;

import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import hazo.hafid.core.Controller;
import hazo.hafid.core.Model;
import java.util.ArrayList;

public class ContatoModel extends Model
{
    public String id;
    public String nome = "";
    public ArrayList<String> telefones = new ArrayList<String>();
    
    public static ContatoModel getByUri( Uri uri )
    {
        Cursor c = Controller.context.getContentResolver().query(uri, null, null, null, null);
        
        int idIndex        = c.getColumnIndex(ContactsContract.Contacts._ID);
        int hasNumberIndex = c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        int nameIndex      = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        
        ContatoModel Contato = new ContatoModel();
        
        while( c.moveToNext() ){
            
            Contato.id   = c.getString(idIndex);
            Contato.nome = c.getString(nameIndex);
            
            if( hasNumberIndex > 0 && c.getInt(hasNumberIndex) > 0){
                Cursor pCur = Controller.context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { Contato.id },null);
                while( pCur.moveToNext() ){
                    Contato.telefones.add( pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)) );
                } 
            }else{
                Contato.telefones = null;
            }
        }
        
        return Contato;
    }
    
}
