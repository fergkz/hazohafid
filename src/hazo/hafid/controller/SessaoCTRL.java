package hazo.hafid.controller;

import android.os.Bundle;
import hazo.hafid.core.Controller;
import hazo.hafid.model.Sessao;

public class SessaoCTRL extends Controller
{   
    public void indexAction(Bundle savedInstanceState)
    {
        if( Sessao.usuarioLogado ){
            finish();
            startAction( MainCTRL.class, "indexAction" );
        }else{
            finish();
            startAction( UsuarioCTRL.class, "loginAction" );
        }
    }
}
