package hazo.hafid.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import hazo.hafid.controller.helper.Sistema;
import java.lang.reflect.Method;

public class Controller extends Activity {

    private Bundle instanceState;
    
    public static Context context;

    private View view;

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {   
        this.instanceState = savedInstanceState;
        super.onCreate(savedInstanceState);
        
        Controller.context = this;

        Intent intent = getIntent();
        String action = intent.getStringExtra("action");
        intent.removeExtra("action");
        
        try{
            if( action == null ){
                
            }else{
                Method m;
                m = this.getClass().getMethod(action, Bundle.class);
                m.invoke(this, this.instanceState);
            }
        }catch( Exception e ){
            Log.d("log", "Exception Controller.onCreate"
                    + " \r\n......... Erro em " + this.getClass().toString()+", Falhar ao carregar '"+action+"'");
        }
    }
    
    public void startAction(Class<?> controllerClass, String action, Integer param)
    {
        try{
            Intent intent = new Intent(this, controllerClass);
            intent.putExtra("action", action);
            intent.putExtra("param", param);
            startActivity(intent);
        }catch( Exception e ){
            Log.d("log", e.getMessage());
        }
    }
    
    public void startAction(Class<?> controllerClass, String action, String param)
    {
        try{
            Intent intent = new Intent(this, controllerClass);
            intent.putExtra("action", action);
            intent.putExtra("param", param);
            startActivity(intent);
        }catch( Exception e ){
            Log.d("log", e.getMessage());
        }
    }
    
    public void startAction(Class<?> controllerClass, String action)
    {
        try{
            Intent intent = new Intent(this, controllerClass);
            intent.putExtra("action", action);
            startActivity(intent);
        }catch( Exception e ){
            Log.d("log", e.getMessage());
        }
    }
    
    public Controller getInstance()
    {
        return this;
    }

    public void setView( int Resource )
    {
//        this.view = getLayoutInflater().inflate(Resource, null);
        this.setContentView(Resource);
    }

    public View getView() {
        return view;
    }
    
    public void message( String message )
    {
        message(message, "E");
    }
    public void message( String message, String type )
    {
        String tipo = type.equals("E") ? "Erro" : "Sucesso";
        Toast.makeText(Controller.context, tipo+": "+message, Toast.LENGTH_SHORT).show();
    }
    
    public void raise( String message )
    {
        this.message(message, "E");
        this.log(message);
    }
    
    public void log( String message )
    {
        Sistema.log(message);
    }
}
